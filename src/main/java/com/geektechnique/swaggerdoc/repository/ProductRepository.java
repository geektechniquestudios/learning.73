package com.geektechnique.swaggerdoc.repository;

import com.geektechnique.swaggerdoc.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface ProductRepository extends JpaRepository<Product, String> {
//    void delete(String id);
//    void findoOne(String id);
}
